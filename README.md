# Instances

This repository contains the instances and the code used in the tests of A.Saken, S.J.Maher,  *Subproblem separation in logic-based Benders' decomposition for the vehicle routing problem with local congesiton*

If you use the instances or code from this repository, please cite
```
@article{2023_saken,
    author = {Saken, A., Maher, S.J.},
    title = {Subproblem separation in logic-based Benders' decomposition for the vehicle routing problem with local congestion},
    editor = {},
    year = {2023},
    pages = {},
    url = {},
    doi = {},
    publisher = {},
}
```

For questions please contact a.saken@exeter.ac.uk.

## Vehicle routing problem with location congestion

The instances used for the vehicle routing problem with location congestion was introduced in E. Lam, G. Gange, P. J. Stuckey, P. Van Hentenryck, and J. J. Dekker. *Nutmeg: A MIP and CP Hybrid Solver Using Branch-and-Check*. SN Operations Research Forum. These instances are available at https://github.com/ed-lam/nutmeg/tree/master/examples/vrplc/Instances.


## Paper

An PDF-version of the preprint can be found at.

## License

The content residing in this repository is licensed under "MIT License", see [LICENSE](LICENSE).

## Acknowledgement

Computational experiments were performed on resources provided by the Swedish National Infrastructure for Computing (SNIC) at National Supercomputer Centre (NSC).
