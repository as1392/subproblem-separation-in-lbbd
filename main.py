"""
A script to test different cuts for logic-based Benders decomposition with subproblem formulation 
for vehicle routing problem.



Usage example: python main.py  -c deletion -o 2 instances/c12j3m1.dat

"""
import math
import random
import argparse
from dataclasses import dataclass, replace
from enum import Enum
from pathlib import Path
from datetime import datetime
from typing import List, Dict, Tuple, Set
import random
from collections import namedtuple, defaultdict
from dfs import Graph
from pprint import pprint
import os

import gurobipy
from docplex.cp.model import CpoModel, start_of, no_overlap, pulse, cumul_range, \
                             SOLVE_STATUS_INFEASIBLE, SOLVE_STATUS_OPTIMAL, FAIL_STATUS_UNKNOWN, presence_of, minimize, start_at_start, start_before_start


class CutStrengthening(Enum):
    NONE = 1
    GREEDY = 2
    DFBS = 3
    ELASTIC = 4
    DELETION = 5
    ADDITIVE = 6
    ADEL = 7


@dataclass(frozen=True)
class Request:
    ID: int
    weight: int
    location: Tuple[int, int]
    locid: int
    equipment_time: int  # included in travel time?
    release_time: int
    deadline: int


@dataclass(frozen=True, order=True)
class Instance:
    ID: str
    Q: int
    T: int
    C: int
    requests: Tuple[Request, ...]



@dataclass(frozen=True, order=True)
class Edge:
    X: int
    Y: int

@dataclass(frozen=True)
class FeasibilityCut:
    ID: int
    edges: Tuple[Edge, ...]

@dataclass(frozen=True)
class OptimalityCut:
    ID: int
    edges: Tuple[Edge, ...]
    makespan: int
    iter_no: int

CutInfo = namedtuple("CutInfo", ["type","no_subproblems", "size"])



def cost(location, location2):
    return math.floor(math.sqrt((location[0] - location2[0]) ** 2 + (location[1] - location2[1]) ** 2))

class InfeasibleError(RuntimeError):
    pass

# Support methods for logic-based Benders decomposition
def master_tardiness(instance: Instance, feasibility_cuts, optimality_cuts, timeout, sub_relaxation=False, valtype=1):
    # Convenience
    requests = instance.requests
    origin = (0, 0)
    
    # Gurobi model
    model = gurobipy.Model()
    model.setParam('TimeLimit', timeout)

    # Variables
    x_dict = dict()
    for request in requests:
        x_dict[request.ID, 0] = model.addVar(name=f"x_({request.ID},0)", vtype=gurobipy.GRB.BINARY)
        x_dict[0, request.ID] = model.addVar(name=f"x_(0,{request.ID})", vtype=gurobipy.GRB.BINARY)
        
        x_dict[request.ID, -1] = model.addVar(name=f"x_({request.ID},-1)", vtype=gurobipy.GRB.BINARY)
        x_dict[-1, request.ID] = model.addVar(name=f"x_(-1,{request.ID})", vtype=gurobipy.GRB.BINARY)
        for request2 in requests:
            if request == request2:
                x_dict[request.ID, request2.ID] = 0
            else:
                x_dict[request.ID, request2.ID] = model.addVar(name=f"x_({request.ID},{request2.ID})", vtype=gurobipy.GRB.BINARY)

     #tardiness T
    T=model.addVar(name="Tardiness T",vtype=gurobipy.GRB.INTEGER)

    #Objective
    model.setObjective(T, sense=gurobipy.GRB.MINIMIZE)

    if sub_relaxation:
        request_id_lookup = dict()
        w_vars = dict()
        t_vars = dict()
        for request in requests + tuple([Request(0, 0, (0, 0),0, 0, 0, instance.T), Request(-1, 0, (0, 0), 0, 0, 0, instance.T)]):
            request_id_lookup[request.ID] = request
            w_vars[request.ID] = model.addVar(name=f"w({request.ID})", vtype=gurobipy.GRB.CONTINUOUS, lb=request.weight, ub=instance.Q)
            t_vars[request.ID] = model.addVar(name=f"t({request.ID})", vtype=gurobipy.GRB.CONTINUOUS, lb=request.release_time)


        for (r1_id, r2_id), v in x_dict.items():
            r1 = request_id_lookup[r1_id]
            r2 = request_id_lookup[r2_id]
            if not isinstance(v, int):
                model.addConstr((v == 1) >> (w_vars[r2.ID] >= w_vars[r1.ID] + r2.weight))
                model.addConstr((v == 1) >> (t_vars[r2.ID] >= t_vars[r1.ID] + r1.equipment_time + cost(r2.location, r1.location)))

    # All requests satisfied
    for request in requests:
        constr_expr = x_dict[request.ID, -1] + x_dict[request.ID, 0] + sum(x_dict[request.ID, request2.ID] for request2 in requests)
        model.addConstr(constr_expr == 1, name=f"RequestFrom({request.ID}")
        constr_expr = x_dict[-1, request.ID] + x_dict[0, request.ID] + sum(x_dict[request2.ID, request.ID] for request2 in requests)
        model.addConstr(constr_expr == 1, name=f"RequestTo({request.ID}")



    # Feasibility cuts
    for feasibility_cut in feasibility_cuts:
        constr_expr = sum(x_dict[edge.X, edge.Y] for edge in feasibility_cut.edges)
        model.addConstr(constr_expr <= len(feasibility_cut.edges) - 1, name=f"feasibility_cut_({feasibility_cut.ID})")

    #Optimality cuts
    if valtype==1:
        # cuts 22 
        for i in set([optimality_cut.iter_no for optimality_cut in optimality_cuts]):
            expr=sum(optimality_cut.makespan*(1-sum((1-x_dict[edge.X, edge.Y]) for edge in optimality_cut.edges)) for optimality_cut in optimality_cuts if optimality_cut.iter_no==i)
            model.addConstr(T>=expr, name=f"cut 2a  iter_{i}")
    if valtype==2 or valtype==4:
        #cuts 23-24
        T_c=dict()
        for optimality_cut in optimality_cuts:
            T_c[optimality_cut.ID,optimality_cut.iter_no]=model.addVar(name=f"Tardiness_component_{optimality_cut.ID}", vtype=gurobipy.GRB.INTEGER)
            constr_expr3=optimality_cut.makespan*(1-sum((1-x_dict[edge.X, edge.Y]) for edge in optimality_cut.edges))
            model.addConstr(T_c[optimality_cut.ID, optimality_cut.iter_no]>=constr_expr3, name=f"T_c (3) optimality cut _{optimality_cut.ID}")
        for i in set([optimality_cut.iter_no for optimality_cut in optimality_cuts]):
            model.addConstr(T>=sum(T_c[optimality_cut.ID,i] for optimality_cut in optimality_cuts if optimality_cut.iter_no==i),name=f"optimality cut(4)_iter{i}")
    if valtype==3 or valtype==4:
        #cuts 27-28
        T_ij=dict()
        for i in requests:
            T_ij[i.ID,0]=model.addVar(name=f"Tardiness_ij_{i.ID}{0}", vtype=gurobipy.GRB.INTEGER)
            T_ij[i.ID,-1]=model.addVar(name=f"Tardiness_ij_{i.ID}{-1}", vtype=gurobipy.GRB.INTEGER)
            T_ij[0,i.ID]=model.addVar(name=f"Tardiness_ij_{0}{i.ID}", vtype=gurobipy.GRB.INTEGER)
            T_ij[-1,i.ID]=model.addVar(name=f"Tardiness_ij_{-1}{i.ID}", vtype=gurobipy.GRB.INTEGER)
            for j in requests:
                if(i.ID!=j.ID):
                    T_ij[i.ID,j.ID]=model.addVar(name=f"Tardiness_ij_{i.ID}{j.ID}", vtype=gurobipy.GRB.INTEGER)
        model.addConstr(T>=sum(T_ij[i.ID,j.ID] for i in requests for j in requests if i.ID!=j.ID)+sum((T_ij[0,i.ID]+T_ij[i.ID,0]+T_ij[-1,i.ID]+T_ij[i.ID,-1]) for i in requests),name=f"sum over the edges")

        for optimality_cut in optimality_cuts:
            constr_expr3=optimality_cut.makespan*(1-sum((1-x_dict[edge.X, edge.Y]) for edge in optimality_cut.edges))
        for i in set([optimality_cut.iter_no for optimality_cut in optimality_cuts]):
            model.addConstr(sum(T_ij[edge.X,edge.Y] for edge in optimality_cut.edges if optimality_cut.iter_no==i)>=constr_expr3, name=f"T_ij (7) optimality cut _{optimality_cut.ID}_{optimality_cut.iter_no}")
            model.addConstr(T>=sum(T_ij[edge.X,edge.Y] for edge  in optimality_cut.edges if optimality_cut.iter_no==i),name=f"optimality cut(7)_sum_iter{i}")


    model.optimize()
    model.write("model.lp")

    try:
        x_sol = {k: v.X if not isinstance(v, int) else v for k, v in x_dict.items()}
    except AttributeError:
        model.computeIIS()
        model.write("model.ilp")
        raise InfeasibleError("Gurobi infeasible")

    return model.objVal, x_sol, model.Runtime


class InfeasibleError(RuntimeError):
    pass


def restricted_sub(instance: Instance, edges: Tuple[Edge, ...], timeout, objtype=0):
    """
    Performs a restricted CP subproblem.
    Raises 'InfeasibleError' if it is Infeasible
    """

    request_ids = set([edge.X for edge in edges] + [edge.Y for edge in edges])
    requests = [r for r in instance.requests if r.ID in request_ids]

    id_to_request = {request.ID: request for request in requests}
    id_to_request[0] = Request(0, 0, (0, 0), 0, 0, 0, instance.T)
    id_to_request[-1] = Request(-1, 0, (0, 0), 0, 0, 0, instance.T)

    model = CpoModel()
    w_vars = dict()
    t_vars = dict()
    for request in requests:
        w_vars[request.ID] = model.interval_var(start=(request.weight, instance.Q), length=request.weight, name=f"w_({request.ID})") 
        t_vars[request.ID] = model.interval_var(size=request.equipment_time, name=f"t_({request.ID})")
        model.add(request.release_time <= start_of(t_vars[request.ID]))
        if objtype!=2:
            model.add(start_of(t_vars[request.ID]) <= request.deadline)
    # 0
    w_vars[0] = model.interval_var(start=(0, instance.Q), name=f"w_(0)")
    t_vars[0] = model.interval_var(name=f"t_(0)")
    model.add(0 <= start_of(t_vars[0]))
    if objtype!=2:
        model.add(start_of(t_vars[0]) <= instance.T)

    # -1
    w_vars[-1] = model.interval_var(start=(0, instance.Q), name=f"w_(-1)")
    t_vars[-1] = model.interval_var(name=f"t_(-1)")
    model.add(0 <= start_of(t_vars[-1]))
    if objtype!=2:
        model.add(start_of(t_vars[-1]) <=instance.T)


    # T constraints
    for edge in edges:
        x_request = id_to_request[edge.X]
        y_request = id_to_request[edge.Y]
        cij = cost(x_request.location, y_request.location)
        model.add(start_before_start(t_vars[edge.X], t_vars[edge.Y], cij + x_request.equipment_time))

    # W constraints
    for edge in edges:
        model.add(start_before_start(w_vars[edge.X], w_vars[edge.Y], id_to_request[edge.Y].weight))

    location_requests_dict = defaultdict(list)
    for request in requests:
        location_requests_dict[request.location].append(request)
    
    for location, requests in location_requests_dict.items():
        location_expr = 0
        for request in requests:
            location_expr += pulse(t_vars[request.ID], 1)    
        model.add(cumul_range(location_expr, 0, instance.C))

    #objective functions for makespan and tardiness 
    if objtype==1:
        model.add(model.minimize(model.max (model.end_of(t_vars[request.ID]) for request in instance.requests)))
    elif objtype==2:
        model.add(model.minimize(model.sum(model.max(model.end_of(t_vars[request.ID])-request.deadline,0) for request in requests )))

    model.export_model()
    if timeout>0:
        msol = model.solve(TimeLimit=timeout, RelativeOptimalityTolerance=0, LogVerbosity='Terse')
    else:
        return "TIMEOUT",None,0.5
    #msol = model.solve(TimeLimit=timeout, RelativeOptimalityTolerance=0, LogVerbosity='Terse')

    if True:
        conflict = model.refine_conflict()
        print(conflict)

    if msol.get_solve_status() == SOLVE_STATUS_INFEASIBLE:
        raise InfeasibleError(f"Infeasible subproblem with edges {edges}")
    elif msol.get_solve_status() == FAIL_STATUS_UNKNOWN:
        return "TIMEOUT",None, msol.get_solve_time()

    elif msol.get_solve_status() == SOLVE_STATUS_OPTIMAL:
        mval = msol.get_objective_values()[0]
        print(type(mval))
        print('Subsol')
        #print("requests",instance.requests)
        msol.write()
        return "OPTIMAL",mval, msol.get_solve_time()

    else:
        return "FEASIBLE", None, msol.get_solve_time()





def deletion_cut_strengthening(instance, edges, sub_timeout: int, valcut=False, objtype=0, objval=0):
    """
    Performs deletion cut-strengthening.
    """
    no_subs = 0
    no_subproblem_timeouts = 0
    refined_edges = list(edges)
    if valcut==False:
        for edge in edges:
            refined_edges.remove(edge)
            try:
                no_subs += 1
                return_code, newobjval,sub_runtime = restricted_sub(instance, refined_edges, sub_timeout)
                sub_timeout=sub_timeout-sub_runtime
                if return_code == "TIMEOUT":
                    no_subproblem_timeouts += 1

                # Add edge if not infeasible
                refined_edges.append(edge)
            except InfeasibleError:
                pass

        print("No. jobs in org cut: ", len(edges))
        print("No. jobs in refined cut: ", len(refined_edges))
        return refined_edges, no_subs, no_subproblem_timeouts
    else:
        #return_code, objval = restricted_sub(instance, refined_edges, sub_timeout, objtype)
        for edge in edges:
            refined_edges.remove(edge)
            no_subs += 1
            return_code, newobjval, sub_runtime = restricted_sub(instance, refined_edges, sub_timeout, objtype)
            sub_timeout=sub_timeout-sub_runtime
            if return_code == "OPTIMAL" and newobjval==objval:
                pass
            else:
                refined_edges.append(edge)
            if return_code == "TIMEOUT":
                no_subproblem_timeouts += 1
        print("No. jobs in org cut: ", len(edges))
        print("No. jobs in refined cut: ", len(refined_edges))
        return refined_edges, no_subs, no_subproblem_timeouts

def additive_cut_strengthening(instance,edges, sub_timeout: int, valcut=False, objtype=0, objval=0):
    """
    Additive method
    """
    #pass
    no_subs = 0
    no_subproblem_timeouts = 0
    T = list()
    S=list(edges)
    I=list()
    if valcut==False:
        while True:
            for edge in S:
                T.append(edge)
                try:
                    no_subs += 1
                    return_code,newobjval, sub_runtime = restricted_sub(instance, T , sub_timeout)
                    sub_timeout=sub_timeout-sub_runtime
                    if return_code == "TIMEOUT":
                        no_subproblem_timeouts += 1

                except InfeasibleError:
                    I.append(edge)
                    S.remove(edge)
                    T=I[:]
                    break
            try:
                 no_subs += 1
                 return_code,newobjval, sub_runtime=restricted_sub(instance,I, sub_timeout)
                 sub_timeout=sub_timeout-sub_runtime
                 if return_code == "TIMEOUT":
                     no_subproblem_timeouts += 1
            except InfeasibleError:
                print("No. jobs in org cut: ", len(edges))
                print("No. jobs in refined cut: ", len(I))
                return I,no_subs, no_subproblem_timeouts
    else:
        #return_code,objval,sub_runtime= restricted_sub(machines, machine_info, machine_jobs, sub_timeout,objtype)
        #sub_timeout=sub_timeout-sub_runtime
        while True:
            for edge in S:
                T.append(edge)
                no_subs += 1
                return_code,newobjval, sub_runtime = restricted_sub(instance, T , sub_timeout, objtype)
                sub_timeout=sub_timeout-sub_runtime
                if return_code == "TIMEOUT":
                    no_subproblem_timeouts += 1
                if return_code == "OPTIMAL" and newobjval == objval:
                    I.append(edge)
                    S.remove(edge)
                    T=I[:]
                    break
            no_subs+= 1
            return_code,newobjval,sub_runtime=restricted_sub(instance, I, sub_timeout, objtype)
            sub_timeout=sub_timeout-sub_runtime
            if return_code == "TIMEOUT":
                no_subproblem_timeouts += 1

            if return_code == "OPTIMAL" and newobjval == objval:
                print("No. jobs in org cut: ", len(edges))
                print("No. jobs in refined cut: ", len(I))
                return I,no_subs, no_subproblem_timeouts

def adel_cut_strengthening(instance,edges, sub_timeout: int, valcut=False, objtype=0, objval=0):
    """
    Additive/Deletion method
    """
    #pass
    no_subs = 0
    no_subproblem_timeouts = 0
    T = list()
    S=list(edges)
    I=list()
    if valcut==False:
        for edge in S:
            T.append(edge)
            try:
                no_subs += 1
                return_code,newobjval,sub_runtime = restricted_sub(instance, T , sub_timeout)
                sub_timeout=sub_timeout-sub_runtime
                if return_code == "TIMEOUT":
                    no_subproblem_timeouts += 1

            except InfeasibleError:
                return deletion_cut_strengthening(instance, T, sub_timeout)
    else:
        #return_code, objval = restricted_sub(instance, edges, sub_timeout, objtype)
        for edge in edges:
            T.append(edge)
            no_subs += 1
            return_code, newobjval, sub_runtime = restricted_sub(instance, T, sub_timeout, objtype)
            sub_timeout=sub_timeout-sub_runtime
            if return_code == "OPTIMAL" and newobjval==objval:
                return deletion_cut_strengthening(instance, T, sub_timeout, objtype,objval)
            if return_code == "TIMEOUT":
                no_subproblem_timeouts += 1


def dfbs_cut_strengthening(instance, edges, sub_timeout: int, valcut=False, objtype=0, objval=0):
    no_subs = 0
    no_subproblem_timeouts = 0
    refined_edges = list(edges)
    T = refined_edges
    I = list()
    S = list()
    T1 = None
    T2 = None
    if valcut==False:
        while True:
            if len(T) <= 1:
                I = list(I) + list(T)
                try:
                    no_subs += 1
                    return_code, newobjval,sub_runtime = restricted_sub(instance, I, sub_timeout)
                    sub_timeout=sub_timeout-sub_runtime
                    if return_code == "TIMEOUT":
                        no_subproblem_timeouts += 1
                except InfeasibleError:
                    print("No. jobs in org cut: ", len(edges))
                    print("No. jobs in refined cut: ", len(I))
                    return I, no_subs, no_subproblem_timeouts
                T = list(S)
                S = list()
                if len(T) >= 2:
                    continue
                T2 = list(T)
                T1 = list()
            else:
                T1 = list(random.sample(T, math.ceil(len(T)/2)))
                T2 = list(set(T).difference(set(T1)))
            try:
                no_subs += 1
                return_code, newobjval, sub_runtime = restricted_sub(instance, I + S + T1, sub_timeout)
                sub_timeout=sub_timeout-sub_runtime
                if return_code == "TIMEOUT":
                    no_subproblem_timeouts += 1
            except InfeasibleError:
                T = list(T1)
            else:
                S = S + T1
                T = list(T2)
    else:
        #return_code,objval = restricted_sub(instance,   T, sub_timeout, objtype)
        while True:
            if len(T) <= 1:
                I = list(I) + list(T)
                no_subs += 1
                return_code,newobjval, sub_runtime = restricted_sub(instance, I, sub_timeout, objtype)
                sub_timeout=sub_timeout-sub_runtime
                if return_code == "TIMEOUT":
                    no_subproblem_timeouts += 1
                if return_code == "OPTIMAL" and newobjval == objval:
                    print("No. jobs in org cut: ", len(edges))
                    print("No. jobs in refined cut: ", len(I))
                    return I, no_subs, no_subproblem_timeouts
                T = list(S)
                S = list()
                if len(T) >= 2:
                    continue
                T2 = list(T)
                T1 = list()
            else:
                T1 = list(random.sample(T, math.ceil(len(T)/2)))
                T2 = list(set(T).difference(set(T1)))
            no_subs += 1
            return_code,newobjval, sub_runtime = restricted_sub(instance,   I + S + T1, sub_timeout, objtype)
            sub_timeout=sub_timeout-sub_runtime
            if return_code == "TIMEOUT":
                no_subproblem_timeouts += 1
            if return_code == "OPTIMAL" and newobjval == objval:
                T = list(T1)
            else:
                S = S + T1
                T = list(T2)


def none_cut_strengthening(instance, edges, sub_timeout: int, valcut=False, objtype=0):
    """
    Performs no cut-strengthening
    """
    return edges, 0, 0


def run_instance(path: Path, sub_relaxation: bool, cut_strengthening: CutStrengthening, sub_timeout: int, valtype: int, objtype: int):
    """
    'run_instance' performs a run of logic-based Benders decomposition according to the specifications.
    """
    print("Instance:", path)
    print("Cut strengthening:", cut_strengthening)
    print("Subproblem relaxation:", sub_relaxation)
    # Parse instance
    with open(path) as f:
        line = f.readline()
        instance_name = line.replace("InstanceName: ", "").replace("\n", "")
        print(f"Instance name: {instance_name}")
        
        line = f.readline()
        T = int(line.split()[1])

        line = f.readline()
        R = int(line.split()[1])

        line = f.readline()
        Q = int(line.split()[1])

        line = f.readline()
        C = int(line.split()[1])

        print(f"T={T}; R={R}; Q={Q}; C={C}")

        # Empty lines
        f.readline()
        f.readline()

        location_dict = {}
        line = f.readline()
        while line.split():
            location_id, x, y = line.split()
            location_dict[location_id] = int(x), int(y)
            print(location_id, int(x), int(y))
            line = f.readline()
        
        # Not interesting line
        f.readline()

        request_id = 1
        line = f.readline()
        requests = list()
        while line.split():
            R, L, A, B, S, Q_r = line.split()
            print(R, L, A, B, S, Q_r)
            print(location_dict[L])
            assert request_id == int(R.replace("R-", ""))
            if L=='L-D':
                num_id=0
            else:
                num_id=int(L.replace("L-",""))
            line = f.readline()
            if objtype==0 or objtype==1:
                request = Request(request_id, int(Q_r), location_dict[L], num_id, int(S), int(A), int(B))
            elif objtype==2:
                request = Request(request_id, int(Q_r), location_dict[L], num_id,  int(S), int(A),int(0.9*int(B)))
            request_id += 1
            requests.append(request)
    
    instance = Instance(instance_name, Q, T, C, tuple(requests))
    print(instance)


    # Start algorithm
    no_iterations = 1
    feasibility_cut_id = 0
    feasibility_cuts = list()
    optimality_cut_id = 0
    optimality_cuts = list()
    org_feas_cuts=list()
    org_opt_cuts=list()
    no_subproblems_performed = 0
    no_subproblem_timeouts = 0
    start_time = datetime.now()
    master_runtime=0
    sub_runtime=0
    sub_totalruntime=0
    it_subtime=0
    sub_values=list()
    cut_info = list()

    # Simple relaxation check
    relax_infeasible = False
    try:
        no_subproblems_performed += 1
        restricted_sub(instance, tuple(), sub_timeout)
    except InfeasibleError:
        relax_infeasible = True
    master_infeasible = False
    timeout = False
    while True and not relax_infeasible:
        print(f"Iteration {no_iterations} with feasibility cuts: {[len(fc.edges) for fc in feasibility_cuts]} and optimality cuts: {[len(oc.edges) for oc in optimality_cuts]}")
        print("Master problem")
        sub_timeout=sub_timeout-it_subtime
        print("ttt", sub_timeout, "subtotal", sub_totalruntime,"master", master_runtime)
        if sub_timeout<0:
            sub_timeout=0.0
            timeout=True
            break
        try:
            if objtype==2:
                obj_val, x_sol, im_runtime = master_tardiness(instance, feasibility_cuts, optimality_cuts, sub_timeout, sub_relaxation, valtype)
        except InfeasibleError:
            master_infeasible = True
            break
        master_runtime+=im_runtime;#adding runtime per iteration to the total master runtime
        sub_timeout=sub_timeout-im_runtime
        print("Master objective",obj_val)
        if(len(sub_values)!=0):
            if(obj_val==min(sub_values)):
                optimal=True
                print(f"Problem finished optimal; (timeout={timeout}) in {no_iterations} iterations! with feasibility cuts: {[len(fc.edges) for fc in feasibility_cuts]} and with optimality cuts: {[len(oc.edges) for oc in optimality_cuts]}")
                break

        print("Sub-problem")
        if sub_timeout<0:
            sub_timeout=0.0
            timeout=True
            break
        infeasible = False
        optimal=False
        timeout = False
        vstar_values=list()
        edges = tuple([Edge(*k) for k, v in x_sol.items() if v])
        print('edges',edges)
        #breakpoint()
        newlist = [[*k] for k, v in x_sol.items() if v]
        # removing the depot for DFS
        edgelist=newlist[:]
        for k in newlist:
            if (k[0]==-1 or k[0]==0) or (k[1]==-1 or k[1]==0):
                edgelist.remove(k)



        #turning eges between request to edges between locations
        loclist=edgelist[:]
        j=0
        for i in edgelist:
            loclist[j]=[requests[i[0]-1].locid,requests[i[1]-1].locid]
            j=j+1

        #adding edges to the graph
        g=Graph(len(location_dict))
        for i in loclist:
            if i[0]!=i[1]:
                g.addEdge(i[0], i[1])

        cc=g.connectedComponents()
        print('Components',cc)

        #splititng by locations


        for loc in cc:
            split_edges=list()
            newreq=list()
            for edge in edges:
                if (edge.X!=-1 and edge.X!=0):
                    if requests[edge.X-1].locid in loc:
                        split_edges.append(edge)
                        newreq.append(requests[edge.X-1])
                elif (edge.Y!=-1 and edge.Y!=0):
                    if requests[edge.Y-1].locid in loc:
                        split_edges.append(edge)
                        newreq.append(requests[edge.Y-1])
            print('loc', loc, 'edges', split_edges)
            if len(split_edges)==0:
                continue




            #######

            ############
            newreq=list(set(newreq))
            split_edges=tuple(split_edges)
            newinstance = Instance(instance_name, Q, T, C, tuple(newreq))

            it_start_time=datetime.now()
            try:
                no_subproblems_performed += 1
                if len(newreq)!=0:
                    return_code, subobj_val, sub_runtime= restricted_sub(newinstance, split_edges, sub_timeout,objtype)
                elif len(newreq)==0:
                    continue
                sub_timeout=sub_timeout-sub_runtime
                if objtype!=0:
                    vstar_values.append(subobj_val)
                if return_code == "TIMEOUT":
                    timeout = True
                    no_subproblem_timeouts += 1
                if return_code=="OPTIMAL" and subobj_val>0:
                    #breakpoint()
                    if cut_strengthening == CutStrengthening.NONE:
                        refined_edges, no_subs, no_sub_timeout = none_cut_strengthening(newinstance, split_edges, sub_timeout, True)
                    elif cut_strengthening == CutStrengthening.GREEDY:
                        refined_edges, no_subs, no_sub_timeout = greedy_cut_strengthening(newinstance, split_edges, sub_timeout,True, objtype, subobj_val)
                    elif cut_strengthening == CutStrengthening.DFBS:
                        refined_edges, no_subs, no_sub_timeout = dfbs_cut_strengthening(newinstance, split_edges, sub_timeout, True, objtype, subobj_val)
                    elif cut_strengthening == CutStrengthening.DELETION:
                        refined_edges, no_subs, no_sub_timeout = deletion_cut_strengthening(newinstance, split_edges, sub_timeout,True, objtype, subobj_val)
                    elif cut_strengthening == CutStrengthening.ADDITIVE:
                        refined_edges, no_subs, no_sub_timeout = additive_cut_strengthening(newinstance, split_edges, sub_timeout, True, objtype, subobj_val)
                    elif cut_strengthening == CutStrengthening.ADEL:
                        refined_edges, no_subs, no_sub_timeout = adel_cut_strengthening(newinstance, split_edges, sub_timeout, True, objtype, subobj_val)
                    else:
                        RuntimeError(f"Cut-strengthening {cut_strengthening} is not available.")
                    cut_info.append(CutInfo('opt',no_subs, len(refined_edges)))
                    no_subproblems_performed += no_subs
                    no_subproblem_timeouts += no_sub_timeout
                    optimality_cut_id += 1
                    for edge1 in split_edges:
                        for edge2 in refined_edges:
                            if edge1!=edge2 and edge1 not in refined_edges:
                                if (edge1.X==edge2.Y) or (edge1.Y==edge2.X):
                                    refined_edges.append(edge1)
                    optimality_cut = OptimalityCut(optimality_cut_id, tuple(refined_edges),subobj_val, no_iterations)
                    optimality_cuts.append(optimality_cut)
                    org_opt_cuts.append(len(split_edges))
            except InfeasibleError:
                infeasible = True
                if cut_strengthening == CutStrengthening.NONE:
                    refined_edges, no_subs, no_sub_timeout = none_cut_strengthening(newinstance, split_edges, sub_timeout)
                elif cut_strengthening == CutStrengthening.GREEDY:
                    refined_edges, no_subs, no_sub_timeout = greedy_cut_strengthening(newinstance,split_edges, sub_timeout)
                elif cut_strengthening == CutStrengthening.DFBS:
                    refined_edges, no_subs, no_sub_timeout = dfbs_cut_strengthening(newinstance, split_edges, sub_timeout)
                elif cut_strengthening == CutStrengthening.DELETION:
                    refined_edges, no_subs, no_sub_timeout = deletion_cut_strengthening(newinstance, split_edges, sub_timeout)
                elif cut_strengthening == CutStrengthening.ADDITIVE:
                    refined_edges, no_subs, no_sub_timeout = additive_cut_strengthening(newinstance, split_edges, sub_timeout)
                elif cut_strengthening == CutStrengthening.ADEL:
                    refined_edges, no_subs, no_sub_timeout = adel_cut_strengthening(newinstance, split_edges, sub_timeout)
                else:
                    RuntimeError(f"Cut-strengthening {cut_strengthening} is not available.")
                cut_info.append(CutInfo('feas',no_subs, len(refined_edges)))
                no_subproblems_performed += no_subs
                no_subproblem_timeouts += no_sub_timeout
                feasibility_cut_id += 1
                feasibility_cut = FeasibilityCut(feasibility_cut_id, tuple(refined_edges))
                feasibility_cuts.append(feasibility_cut)
                org_feas_cuts.append(len(edges))
                if False:
                    print(feasibility_cut)
                    request_ids = set([edge.X for edge in feasibility_cut.edges] + [edge.Y for edge in feasibility_cut.edges])
                    requests = list()
                    for request in instance.requests:
                        if request.ID in request_ids:
                            print(request)
                            requests.append(request)

                    if len(requests) < 2:
                        print("cost")
                        print(cost(requests[0].location, (0, 0)))
                    else:
                        print("cost")
                        print(cost(requests[0].location, requests[1].location))

        sub_totalruntime+=(datetime.now()-it_start_time).total_seconds()
        it_subtime=(datetime.now()-it_start_time).total_seconds()
        if (len(vstar_values)!=0 and infeasible==False):
            if objtype==1:
                sub_values.append(max(vstar_values))
            if objtype==2:
                sub_values.append(sum(vstar_values))
            print("Subvalue",sub_values)
            print("Vstar", vstar_values)
            if obj_val==min(sub_values):
                optimal=True

        if  optimal:
            print(f"Problem finished optimal; (timeout={timeout}) in {no_iterations} iterations! with feasibility cuts: {[len(fc.edges) for fc in feasibility_cuts]} and with optimality cuts: {[len(oc.edges) for oc in optimality_cuts]}")
            plot_graph = False
            if plot_graph:
                print(x_sol)
                print(edges)
                import networkx as nx
                import matplotlib.pyplot as plt
                graph = nx.Graph()
                for edge in edges:
                    graph.add_edge(edge.X, edge.Y)
                nx.draw(graph, with_labels=True, font_weight='bold')
                if cut_strengthening == CutStrengthening.DFBS:
                    plt.savefig(f"{instance.ID}.png")
                else:
                    plt.savefig(f"GREEDY/{instance.ID}.png")



            break

        elif  objtype==0 and not infeasible:
            print(f"Problem finished; (timeout={timeout}) in {no_iterations} iterations! with feasibility cuts: {[len(fc.edges) for fc in feasibility_cuts]} and with optimality cuts: {[len(oc.edges) for oc in optimality_cuts]}")

            break
        else:
            no_iterations += 1

        if not infeasible:
            plot_graph = False
            if plot_graph:
                print(x_sol)
                print(edges)
                import networkx as nx
                import matplotlib.pyplot as plt
                graph = nx.Graph()
                for edge in edges:
                    graph.add_edge(edge.X, edge.Y)
                nx.draw(graph, with_labels=True, font_weight='bold')
                if cut_strengthening == CutStrengthening.DFBS:
                    plt.savefig(f"{instance.ID}.png")
                else:
                    plt.savefig(f"{instance.ID}.png")



    time_spent = (datetime.now() - start_time).total_seconds()
    print("Time spent:", time_spent)
    if feasibility_cuts:
        avg_feas_cut_size = sum(len(fc.edges) for fc in feasibility_cuts) / len(feasibility_cuts)
        org_feas_cuts_size=sum(org_feas_cuts)/len(org_feas_cuts)
        no_feas_cuts=len(feasibility_cuts)
    else:
        avg_feas_cut_size = -1
        org_feas_cuts_size= -1
        no_feas_cuts=0
    if optimality_cuts:
        avg_opt_cut_size = sum(len(fc.edges) for fc in optimality_cuts) / len(optimality_cuts)
        org_opt_cuts_size=sum(org_opt_cuts)/len(org_opt_cuts)
        no_opt_cuts=len(optimality_cuts)
    else:
        avg_opt_cut_size = -1
        org_opt_cuts_size= -1
        no_opt_cuts=0
    if timeout:
        return "TIMEOUT", time_spent, sub_runtime, master_runtime, no_subproblems_performed, no_subproblem_timeouts, no_iterations, avg_feas_cut_size, avg_opt_cut_size,org_feas_cuts_size,org_opt_cuts_size, no_feas_cuts, no_opt_cuts, cut_info
    elif relax_infeasible:
        return "RELAX_INFEASIBLE", time_spent, sub_runtime, master_runtime, no_subproblems_performed, no_subproblem_timeouts, no_iterations, avg_feas_cut_size, avg_opt_cut_size,org_feas_cuts_size,org_opt_cuts_size, no_feas_cuts, no_opt_cuts, cut_info
    elif master_infeasible:
        return "MASTER_INFEASIBLE", time_spent, sub_runtime, master_runtime, no_subproblems_performed, no_subproblem_timeouts, no_iterations, avg_feas_cut_size, avg_opt_cut_size,org_feas_cuts_size,org_opt_cuts_size, no_feas_cuts, no_opt_cuts, cut_info
    else:
        return obj_val, time_spent, sub_runtime, master_runtime, no_subproblems_performed, no_subproblem_timeouts, no_iterations, avg_feas_cut_size, avg_opt_cut_size,org_feas_cuts_size,org_opt_cuts_size, no_feas_cuts, no_opt_cuts, cut_info


if __name__ == "__main__":
    # Argument parser
    parser = argparse.ArgumentParser(description='Perform logic-based Benders decomposition tests')
    parser.add_argument('-r', '--sub_relaxation', action='store_true', help='Specify if subproblem relaxation should be used for the methods.')
    parser.add_argument('-c', '--cut_strengthening', default="elastic", choices=["none", "greedy", "dfbs", "elastic", "deletion","additive","adel"], type=str.lower, help='Specify the type of cut-strengthening to be applied.\n\t - none: no cut-strengthening\n\t - greedy: cut-strengthening where a task is removed in each iteration until feasibility occurs\n\t - dfbs: cut-strengthening that creates an irreducible cut with best-first depth-first (dfbs) strategy')
    parser.add_argument('-s', '--seed', help="The seed used for randomisation", default=0, type=int)
    parser.add_argument('-t', '--sub_timeout', help="The timeout used for a subproblem", default=999999999, type=int)
    parser.add_argument('--csv', help="Appends the results of the performed instance into a specified csv-file")
    parser.add_argument('-v', '--value_cut_type', default=0, help="The type of generated value cuts :0 strenthened nogood cuts, 1:analytic cuts", type=int)
    parser.add_argument('-o', '--objective_type', default=0, help="The type of the objective function: 0- minimising travel time,1- minimising makespan ,2- minimising tardiness", type=int)
    parser.add_argument('instance', help='Path to instance that is tested', type=str)
    args = parser.parse_args()

    cut_strengthening = None
    if args.cut_strengthening == "none":
        cut_strengthening = CutStrengthening.NONE
    elif args.cut_strengthening == "greedy":
        cut_strengthening = CutStrengthening.GREEDY
    elif args.cut_strengthening == "dfbs":
        cut_strengthening = CutStrengthening.DFBS
    elif args.cut_strengthening == "elastic":
        cut_strengthening = CutStrengthening.ELASTIC
    elif args.cut_strengthening == "deletion":
        cut_strengthening = CutStrengthening.DELETION
    elif args.cut_strengthening == "additive":
        cut_strengthening = CutStrengthening.ADDITIVE
    elif args.cut_strengthening == "adel":
        cut_strengthening = CutStrengthening.ADEL

    random.seed(args.seed)
    obj_val, time_spent, sub_runtime, master_runtime, no_subproblems_performed, no_subproblem_timeouts, no_benders_iterations, avg_feas_cut_size,avg_opt_cut_size, org_feas_cuts_size,org_opt_cuts_size, no_feas_cuts, no_opt_cuts, cut_info = run_instance(Path(args.instance), args.sub_relaxation, cut_strengthening, args.sub_timeout, args.value_cut_type, args.objective_type)

    avg_cut_size=avg_opt_cut_size
    first_row = "instance,seed,cut_strengthening,sub_relaxation,sub_timeout,time_spent,sub_runtime,master_runtime,no_subproblems_performed,no_subproblem_timeouts,no_benders_iteration,avg_feas_cut_size,avg_opt_cut_size, org_feas_cuts_size,org_opt_cuts_size, no_feas_cuts, no_opt_cuts,obj_val\n"
    info_row = f'"{args.instance}",{args.seed},"{args.cut_strengthening}",{int(args.sub_relaxation)},{args.sub_timeout},{time_spent},{sub_runtime},{master_runtime},{no_subproblems_performed},{no_subproblem_timeouts},{no_benders_iterations},{avg_feas_cut_size},{avg_opt_cut_size},{org_feas_cuts_size},{org_opt_cuts_size},{no_feas_cuts},{no_opt_cuts},{obj_val}\n'
    print(info_row)
    exists = False
    if args.csv:
        if os.path.exists(args.csv):
            exists = True
        with open(args.csv, 'a') as csv_file:
            # Write "info row" if file does not exists beforehand
            if not exists:
                csv_file.write(first_row)
            csv_file.write(info_row)

        # Write cut csv file
        cut_csv = args.csv.replace(".csv", "_cut.csv")
        first_row = "instance,seed,cut_strengthening,no_subproblems,cut_size\n"
        exists = False
        if os.path.exists(cut_csv):
            exists = True
        with open(cut_csv, 'a') as csv_file:
            # Write "info row" if file does not exists beforehand
            if not exists:
                csv_file.write(first_row)
            for cut in cut_info:
                info_row = f'"{args.instance}",{args.seed},"{args.cut_strengthening}",{cut.no_subproblems},{cut.size}, {cut.type}\n'
                csv_file.write(info_row)

# python main.py instances/vrplc13_11_10_s10.txt -r -c deletion -o 2 --csv test.csv
